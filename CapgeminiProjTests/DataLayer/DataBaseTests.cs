﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CapgeminiProj.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapgeminiProj.Model;

namespace CapgeminiProj.DataLayer.Tests
{
    [TestClass()]
    public class DataBaseTests
    {
        [TestMethod()]
        public void AddCustomerTest()
        {
            Customer testCustomer = new Customer("Adam", "Kornacki", "504-225-221", "Gdziestam 101");

            DataBase DB = new DataBase();

            DB.AddCustomer(testCustomer);

            Assert.AreEqual(1, DB.GiveListOfCustomers().Count());

        }

        [TestMethod()]
        public void RemoveCustomerTest()
        {
            Customer testCustomer = new Customer("Adam", "Kornacki", "504-225-221", "Gdziestam 101");

            DataBase DB = new DataBase();

            DB.AddCustomer(testCustomer);
            DB.AddCustomer(testCustomer);
            DB.AddCustomer(testCustomer);
            DB.AddCustomer(testCustomer);

            DB.RemoveCustomer(2);

            Assert.AreEqual(3, DB.GiveListOfCustomers().Count());
        }

        [TestMethod()]
        public void GetCustomerAtIndexTest()
        {
            Customer testCustomer = new Customer("Adam", "Kornacki", "504-225-221", "Gdziestam 101");
            Customer testCustomerToGet = new Customer("Adam", "Nawalka", "504-225-221", "Gdziestam 101");

            Customer testCustomerCheck = new Customer("Adam", "Nawalka", "504-225-221", "Gdziestam 101");

            DataBase DB = new DataBase();

            DB.AddCustomer(testCustomer);
            DB.AddCustomer(testCustomerToGet);

            Customer gotCustomer = DB.GetCustomerAtIndex(1);

            Assert.AreEqual(testCustomerCheck.Surname, gotCustomer.Surname);            
        }

        [TestMethod()]
        public void EditCustomerTest()
        {
            Customer testCustomer = new Customer("Adam", "Kornacki", "504-225-221", "Gdziestam 101");

            Customer testCustomerCheck = new Customer("Adam", "Nawalka", "504-225-221", "Gdziestam 101");

            DataBase DB = new DataBase();

            DB.AddCustomer(testCustomer);

            DB.EditCustomer(0, "", "Nawalka", "", "");

            Customer gotCustomer = DB.GetCustomerAtIndex(0);

            Assert.AreEqual(testCustomerCheck.Surname, gotCustomer.Surname);
        }

        [TestMethod()]
        public void GiveListOfCustomersTest()
        {
            Customer testCustomer = new Customer("Adam", "Kornacki", "504-225-221", "Gdziestam 101");
            Customer testCustomer2 = new Customer("Adam", "Nawalka", "504-225-221", "Gdziestam 101");

            List<Customer> listOfCustomers = new List<Customer>();
            List<Customer> listOfCustomersFromDB = new List<Customer>();

            listOfCustomers.Add(testCustomer);
            listOfCustomers.Add(testCustomer2);

            DataBase DB = new DataBase();

            DB.AddCustomer(testCustomer);
            DB.AddCustomer(testCustomer2);

            listOfCustomersFromDB = DB.GiveListOfCustomers();

            Assert.AreEqual(listOfCustomersFromDB[1].Surname, listOfCustomers[1].Surname);
        }
    }
}