﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CapgeminiProj.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapgeminiProj.Model.Tests
{
    [TestClass()]
    public class CustomerTests
    {
        [TestMethod()]
        public void ToStringTest()
        {
            Customer testCustomer = new Customer("Adam", "Kornacki", "504-225-221", "Gdziestam 101");

            string returnValue = testCustomer.ToString();

            Assert.AreEqual(returnValue, "Kornacki");
        }
    }
}