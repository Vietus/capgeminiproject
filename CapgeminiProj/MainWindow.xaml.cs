﻿using System;
using System.Windows;
using CapgeminiProj.DataLayer;
using CapgeminiProj.Model;

namespace CapgeminiProj
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ClearTextboxes()
        {
            TbxName.Text = "";
            TbxSurname.Text = "";
            TbxAdress.Text = "";
            TbxPhone.Text = "";
        }

        private void BtnAddCustomer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (TbxName.Text == "" || TbxSurname.Text == "" || TbxAdress.Text == ""
                    || TbxPhone.Text == "")
                {
                    MessageBox.Show("Fill all data to add user!");
                }
                else
                {
                    Customer newCustomer = new Customer(TbxName.Text, TbxSurname.Text,
                        TbxPhone.Text, TbxAdress.Text);

                    CustomerDB.Instance.dataBaseObiect.AddCustomer(newCustomer);

                    CBCustomers.Items.Refresh();

                    string info = String.Format("Customer {0} was added!", TbxSurname.Text);
                    MessageBox.Show(info);

                    ClearTextboxes();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error!");
            }
        }

        private void BtnEditCustomer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (TbxName.Text == "" || TbxSurname.Text == "" || TbxAdress.Text == ""
                    || TbxPhone.Text == "")
                {
                    MessageBox.Show("Select Customer to edit user!");
                }
                else
                {
                    int editCustomerIndex = CBCustomers.SelectedIndex;

                    CustomerDB.Instance.dataBaseObiect.EditCustomer(editCustomerIndex,
                        TbxName.Text, TbxSurname.Text, TbxPhone.Text, TbxAdress.Text);

                    CBCustomers.Items.Refresh();

                    string info = String.Format("Customer {0} updated!", TbxSurname.Text);
                    MessageBox.Show(info);


                    CBCustomers.SelectedItem = null;
                    ClearTextboxes();
                }    
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error!");
            }
        }

        private void CBCustomers_Loaded(object sender, RoutedEventArgs e)
        {
            CBCustomers.ItemsSource = CustomerDB.Instance.dataBaseObiect.GiveListOfCustomers();
        }

        private void BtnRemoveCustomer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int removeCustomerIndex = CBCustomers.SelectedIndex;

                CustomerDB.Instance.dataBaseObiect.RemoveCustomer(removeCustomerIndex);

                CBCustomers.Items.Refresh();

                string info = String.Format("Customer {0} was deleted!", TbxSurname.Text);

                MessageBox.Show(info);

                ClearTextboxes();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error!");
            }
        }

        private void BtnSelectCustomer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CBCustomers.SelectedItem != null)
                {
                    Customer selectedCustomer = (Customer)CBCustomers.SelectedItem;

                    TbxName.Text = selectedCustomer.Name;
                    TbxSurname.Text = selectedCustomer.Surname;
                    TbxAdress.Text = selectedCustomer.Adress;
                    TbxPhone.Text = selectedCustomer.TelephoneNumber;
                }
                else
                {
                    MessageBox.Show("Please, select a Customer!");
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Error!");
            }
        }
    }
}
