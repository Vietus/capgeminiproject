﻿namespace CapgeminiProj.DataLayer
{
    public class CustomerDB
    {
        private static CustomerDB dataBase = new CustomerDB();
        private CustomerDB() { }

        public DataBase dataBaseObiect = new DataBase();

        public static CustomerDB Instance
        {
            get { return dataBase; }

        }
    }
}
