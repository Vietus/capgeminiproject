﻿using CapgeminiProj.Model;
using System.Collections.Generic;

namespace CapgeminiProj.DataLayer
{
    public class DataBase
    {
        private List<Customer> listOfCustomers;

        public DataBase()
        {
            listOfCustomers = new List<Customer>();
        }

        public void AddCustomer(Customer customer)
        {
            listOfCustomers.Add(customer);
        }

        public void RemoveCustomer(int index)
        {
            listOfCustomers.RemoveAt(index);
        }

        public Customer GetCustomerAtIndex(int index)
        {
            return listOfCustomers[index];
        }

        public void EditCustomer(int index, string name = "", string surname = "",
            string phone = "", string adress = "")
        {
            if (name != "")
            {
                listOfCustomers[index].Name = name;
            }
            if (surname != "")
            {
                listOfCustomers[index].Surname = surname;
            }
            if (phone != "")
            {
                listOfCustomers[index].TelephoneNumber = phone;
            }
            if (adress != "")
            {
                listOfCustomers[index].Adress = adress;
            }
        }

        public List<Customer> GiveListOfCustomers()
        {
            return listOfCustomers;
        }
    }
}
