﻿namespace CapgeminiProj.Model
{
    public class Customer
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string TelephoneNumber { get; set; } 
        public string Adress { get; set; }

        public Customer (string name, string surname, string phone, string adress)
        {
            Name = name;
            Surname = surname;
            TelephoneNumber = phone;
            Adress = adress;
        }

        public override string ToString()
        {
            return Surname;
        }
    }
}
